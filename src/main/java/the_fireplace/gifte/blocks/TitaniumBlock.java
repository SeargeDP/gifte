package the_fireplace.gifte.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class TitaniumBlock extends Block {

	public TitaniumBlock(Material p_i45394_1_) {
		super(p_i45394_1_);
		setBlockTextureName("gifte:titanium_block");
		setBlockName("TitaniumBlock");
		setCreativeTab(CreativeTabs.tabBlock);
        setHardness(10.0F);
	}

}
