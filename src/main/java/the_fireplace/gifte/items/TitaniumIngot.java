package the_fireplace.gifte.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class TitaniumIngot extends Item {
	public TitaniumIngot(){
		setTextureName("gifte:titanium_ingot");
		setUnlocalizedName("TitaniumIngot");
		setCreativeTab(CreativeTabs.tabMaterials);
	}

}
