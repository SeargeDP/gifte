package the_fireplace.gifte.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class BlackLightDust extends Item {
	public BlackLightDust(){
		setTextureName("gifte:blacklight_dust");
		setUnlocalizedName("BlacklightDust");
		setCreativeTab(CreativeTabs.tabMaterials);
	}

}
