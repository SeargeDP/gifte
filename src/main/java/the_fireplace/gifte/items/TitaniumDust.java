package the_fireplace.gifte.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class TitaniumDust extends Item {
public TitaniumDust(){
	setTextureName("gifte:titanium_dust");
	setUnlocalizedName("TitaniumDust");
	setCreativeTab(CreativeTabs.tabMaterials);
}
}
