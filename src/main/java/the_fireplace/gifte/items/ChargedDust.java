package the_fireplace.gifte.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ChargedDust extends Item {
	public ChargedDust(){
		setTextureName("gifte:charged_dust");
		setUnlocalizedName("ChargedDust");
		setCreativeTab(CreativeTabs.tabMaterials);
	}

}
