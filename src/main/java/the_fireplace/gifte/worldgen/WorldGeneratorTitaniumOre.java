package the_fireplace.gifte.worldgen;

import java.util.Random;

import the_fireplace.gifte.GIFTEBase;
import the_fireplace.gifte.blocks.TitaniumOre;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import cpw.mods.fml.common.IWorldGenerator;

public class WorldGeneratorTitaniumOre implements IWorldGenerator {
    public void generateSurface(World world, java.util.Random rand, int chunkX, int chunkZ){
        for(int i = 0; i < 11; i++){
            int randPosX = chunkX + rand.nextInt(16);
            int randPosY = rand.nextInt(25)+10;
            int randPosZ = chunkZ + rand.nextInt(16);
            new WorldGenMinable(GIFTEBase.TitaniumOre, 16).generate(world, rand, randPosX, randPosY, randPosZ);}

}


	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world,
			IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
		generateSurface(world, random, chunkZ, chunkZ);
		
	}}