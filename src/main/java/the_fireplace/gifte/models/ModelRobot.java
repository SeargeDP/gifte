package the_fireplace.gifte.models;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelRobot extends ModelBase
{
  //fields
    ModelRenderer Shape1;
    ModelRenderer Shape2;
    ModelRenderer Shape3;
    ModelRenderer Shape4;
    ModelRenderer Shape5;
    ModelRenderer Shape6;
    ModelRenderer Shape62;
    ModelRenderer Shape7;
    ModelRenderer Shape72;
    ModelRenderer Shape8;
  
  public ModelRobot()
  {
    textureWidth = 128;
    textureHeight = 64;
    
      Shape1 = new ModelRenderer(this, 96, 0);
      Shape1.addBox(0F, 0F, 0F, 8, 8, 8);
      Shape1.setRotationPoint(-2F, -11F, -3F);
      Shape1.setTextureSize(128, 64);
      Shape1.mirror = true;
      setRotation(Shape1, 0F, 0F, 0F);
      Shape2 = new ModelRenderer(this, 90, 0);
      Shape2.addBox(0F, 0F, 0F, 1, 12, 2);
      Shape2.setRotationPoint(0.5F, 10F, 0F);
      Shape2.setTextureSize(128, 64);
      Shape2.mirror = true;
      setRotation(Shape2, 0F, 0F, 0F);
      Shape3 = new ModelRenderer(this, 84, 0);
      Shape3.addBox(0F, 0F, 0F, 1, 12, 2);
      Shape3.setRotationPoint(2.5F, 10F, 0F);
      Shape3.setTextureSize(128, 64);
      Shape3.mirror = true;
      setRotation(Shape3, 0F, 0F, 0F);
      Shape4 = new ModelRenderer(this, 72, 0);
      Shape4.addBox(0F, 0F, 0F, 1, 5, 5);
      Shape4.setRotationPoint(1.5F, 19F, -1.5F);
      Shape4.setTextureSize(128, 64);
      Shape4.mirror = true;
      setRotation(Shape4, 0F, 0F, 0F);
      Shape5 = new ModelRenderer(this, 46, 0);
      Shape5.addBox(0F, 0F, 0F, 8, 12, 4);
      Shape5.setRotationPoint(-2F, -2F, -1F);
      Shape5.setTextureSize(128, 64);
      Shape5.mirror = true;
      setRotation(Shape5, 0F, 0F, 0F);
      Shape6 = new ModelRenderer(this, 30, 0);
      Shape6.addBox(0F, 0F, 0F, 4, 4, 4);
      Shape6.setRotationPoint(6F, -2F, -1F);
      Shape6.setTextureSize(128, 64);
      Shape6.mirror = true;
      setRotation(Shape6, 0F, 0F, 0F);
      Shape62 = new ModelRenderer(this, 30, 8);
      Shape62.addBox(0F, 0F, 0F, 4, 4, 4);
      Shape62.setRotationPoint(-6F, -2F, -1F);
      Shape62.setTextureSize(128, 64);
      Shape62.mirror = true;
      setRotation(Shape62, 0F, 0F, 0F);
      Shape7 = new ModelRenderer(this, 22, 0);
      Shape7.addBox(0F, 0F, 0F, 2, 10, 2);
      Shape7.setRotationPoint(6F, -1F, 0F);
      Shape7.setTextureSize(128, 64);
      Shape7.mirror = true;
      setRotation(Shape7, 0F, 0F, -0.0872665F);
      Shape72 = new ModelRenderer(this, 13, 0);
      Shape72.addBox(0F, 0F, 0F, 2, 10, 2);
      Shape72.setRotationPoint(-4F, -1F, 0F);
      Shape72.setTextureSize(128, 64);
      Shape72.mirror = true;
      setRotation(Shape72, 0F, 0F, 0.0872665F);
      Shape8 = new ModelRenderer(this, 6, 0);
      Shape8.addBox(0F, 0F, 0F, 2, 22, 1);
      Shape8.setRotationPoint(1F, -11F, 0F);
      Shape8.setTextureSize(128, 64);
      Shape8.mirror = true;
      setRotation(Shape8, 0F, 0F, 0F);
  }
  
  public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
  {
    super.render(entity, f, f1, f2, f3, f4, f5);
    setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    Shape1.render(f5);
    Shape2.render(f5);
    Shape3.render(f5);
    Shape4.render(f5);
    Shape5.render(f5);
    Shape6.render(f5);
    Shape6.render(f5);
    Shape7.render(f5);
    Shape7.render(f5);
    Shape8.render(f5);
  }
  
  private void setRotation(ModelRenderer model, float x, float y, float z)
  {
    model.rotateAngleX = x;
    model.rotateAngleY = y;
    model.rotateAngleZ = z;
  }
  
  public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
  {
    super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
  }

}
